function ADSaddMatlabOutput(fid,MATfilename,customFilter)
% this function adds the statements for the MatlabOutput to an ADS netlist
%
%   ADSaddMatlabOutput(fid,MATfilename)
%   ADSaddMatlabOutput(fid,MATfilename,customFilter)
%
% You can specify a custom filter to select the variable names that will be
% placed in the generated mat file. Check the ADS help on MatlabOutput to
% find more info on these filters.
%
% Adam Cooman ELEC VUB
% 17/05/2013 Version 1.0
% 30/07/2014 Added the custom filtering option
fprintf(fid,'\r\n');
fprintf(fid,'#load "python","PostProcessing"\r\n');
if exist('customFilter','var')
    if ~isempty(customFilter)
        fprintf(fid,'Controller Module="PostProcessing" \\ \r\n');
        fprintf(fid,'PostProcessingModule="MatlabOutput"  \\ \r\n');
        fprintf(fid,'Argument[0] = "%s"  \\ \r\n',MATfilename);
        fprintf(fid,'Argument[1] = "All"  \\ \r\n');
        fprintf(fid,'Argument[2] = yes  \\  \r\n');
        fprintf(fid,'Argument[3] = "%s"  \r\n',customFilter);
    else
        fprintf(fid,'Controller Module="PostProcessing" PostProcessingModule="MatlabOutput" Argument[0]="%s" Argument[1]="All" Argument[2]=no Argument[3]=""\r\n',MATfilename);
    end
    
else
    fprintf(fid,'Controller Module="PostProcessing" PostProcessingModule="MatlabOutput" Argument[0]="%s" Argument[1]="All" Argument[2]=no Argument[3]=""\r\n',MATfilename);
end

% Example of a MatlabOutput block with custom filtering expression
% #load "python","PostProcessing"
% Controller Module="PostProcessing" \ 
% PostProcessingModule="MatlabOutput" \ 
% Argument[0] = "ADS.mat" \ 
% Argument[1] = "All" \ 
% Argument[2] = yes \  
% Argument[3] = "V_*;I_*" 

end