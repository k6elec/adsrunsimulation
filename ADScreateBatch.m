function j=ADScreateBatch(fhandle,NetlistFileName,MSdef,varargin)

p = inputParser();
p.KeepUnmatched = true;
% fHandle is the function handle containing one of the following
% possibilities;
%   - ADSrunSimulation
%   - ADSsimulateMS
%   - ADSsimulateWaves
%   - ...
p.addRequired('fhandle',@(x) strcmpi(class(x),'function_handle'));
% NetlistFilename is the filename of the netlist
p.addRequired('NetlistFileName',@(x) ischar(x)|iscell(x));
% MSdef is the multisine definition, see ADSsimulateMS for the details
p.addRequired('MSdef',@isstruct);
% File location where the diary will be saved, default is in simulation folder
p.addParamValue('diaryLocation',[],@ischar);
% File location where the output data will be saved, default is in the
% current folder.
p.addParamValue('dataLocation',[],@ischar);

p.parse(fhandle,NetlistFileName,MSdef,varargin{:});
args = p.Results;

c = parcluster;

% if isempty(args.diaryFile)
%     diary on;
% else
%     diary on;
%     diary(args.diaryFile);
% end

%Set up the batch
if isequal(fhandle,@ADSrunSimulation)
    j=batch(c,fhandle,1,{args.NetlistFileName,p.Unmatched},'CaptureDiary',true);
else
    j=batch(c,fhandle,1,{args.NetlistFileName,args.MSdef,p.Unmatched},'CaptureDiary',true);
end

end