% This demo shows how to perform a basic simulation in ADS from MATLAB.
% 
% The netlist in the file 'testNetlist.txt' will be simulated.
% This demo netlist contains a pi network of a coil and two capacitors with
% 50 Ohm terminations left and right:
%
%         o------o--coil---o------o
%         |      |         |      |
%        Term   cap       cap    Term
%         |      |         |      |
%        gnd    gnd       gnd    gnd
%
% capacitors are 1pF and the coil is 1nH
clear variables
close all
clc

% set the preferences of the matlab right. This has to be done only once on
% each computer. You'll have to edit this file to set the configuration
% right for your computer and Z-drive
% set_preferences_ADS2014

% run the netlist with ADSrunSimulation
simresult=ADSrunSimulation('Netlist.net');

% simresult is a struct which contains the simulation results.
% There is only one simulation performed on the netlist, so the struct contains only one field: sim1_S
% sim1_S is again a struct which contains the specific results of the S-parameter simulation
% its fields are
%    freq       1xF vector which contains the frequencies at which the S-parameters were determined
%    Z0         NxNxF matrix which contains a diagonal matrix at each frequency with the characteristic impedance of the port
%    S          NxNxF matrix which contains the S-parameter matrix for each frequency

% as an example, we plot the S(2,1) and S(1,1) as a function of frequency
plot(simresult.sim1_S.freq,db(squeeze(simresult.sim1_S.S(2,1,:))))
hold on
plot(simresult.sim1_S.freq,db(squeeze(simresult.sim1_S.S(1,1,:))),'r')
legend('S21','S11')
